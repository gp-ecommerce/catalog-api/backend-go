package domain

import "fmt"

// Generic representation of a product.
// The categorical particularity of each product can be defined via the Feature attribute
type Product struct {
	Name             string                 `json:"name" bson:"name"`
	Id               string                 `json:"id" bson:"id"`
	Description      string                 `json:"description" bson:"description"`
	ShortDescription string                 `json:"short_description" bson:"short_description"`
	Feature          map[string]interface{} `json:"feature" bson:"feature"`
	NumberOfUnit     uint                   `json:"number_of_unit" bson:"number_of_unit"`
	Image            map[string]string      `json:"images_url" bson:"images_url"`
	Price            Price                  `json:"price" bson:"price"`
	Datebought       int64                  `json:"date_bought" bson:"date_bought"`
}

type Price struct {
	Price    int    `json:"price" bson:"price"`
	Currency string `json:"currency" bson:"currency"`
}

func (p Product) String() string {
	return fmt.Sprintf("{name:'%v', Id:'%v'}", p.Name, p.Id)
}
