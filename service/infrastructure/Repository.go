package infrastructure

import (
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/domain"
)

// Interface to interact with the product repository
// all error from method can be cast into Error apart when mention
type Repository interface {
	GetByShopCategory(string, uint) ([]domain.Product, error)
	GetByName(string, uint) ([]domain.Product, error)
	GetByID(string) (domain.Product, error)
	GetProductCategories(*domain.Product) ([]string, error)
	// Return products with a name close to the query
	SearchName(string, uint) ([]domain.Product, error)
	// Add product to the repository if it is not present inside the database
	// return the product that haven't been able to be inserted inside the repository
	// the error can be cast into batchError
	AddProducts(...*domain.Product) error
	// the error can be cast into batchError
	DeleteProducts(...*domain.Product) error
	AddCategories(*domain.Product, ...string) error
	RemoveCategories(*domain.Product, ...string) error
	ClearCategories(*domain.Product) error
	Len() (uint, error)
	Clear() error
	All() ([]domain.Product, error)
	Ping() error
}
