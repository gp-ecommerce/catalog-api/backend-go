package infrastructure

import (
	"fmt"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/domain"
)

// Describe the error of the repository, respected the error interface
type Error struct {
	// High level error message
	Message string
	// Detail error message relative to the implementation
	Detail    string
	Intensity Intensity
}

func (e Error) Error() string {
	return fmt.Sprintf("%#v", e)
}

const NOT_ABLE_TO_CONNECT = "was not able to connect"
const NOT_ABLE_TO_QUERY = "was not able to query the repository"
const NO_RESULT_FROM_QUERY = "no result from the query"
const NOT_ABLE_TO_DESERIALIZED = "was not able to deserialized"
const NOT_ABLE_TO_INSTANTIATE_THE_REPOSITORY = "was not able to instantiate the repository"
const NOT_ABLE_TO_ADD_EVERY_PRODUCTS = "was not able to add all the product"
const NOT_ABLE_TO_SETUP = "was not able to setup the repository"
const NOT_ABLE_TO_DELETE_EVERY_PRODUCTS = "was not able to delete every products"
const NOT_ABLE_TO_ADD_CATEGORIES = "was not able to had the categories to the product"
const NOT_ABLE_TO_REMOVE_CATEGORIES = "was not able to remove the categories to the product"
const NOT_ABLE_TO_CLEAR_CATEGORIES = "was not able to clear the categories"
const NOT_ABLE_TO_CLEAR_REPOSITORY = "was not able to clear the repository"
const NOT_ABLE_TO_ADD_A_PRODUCT = "was not able to add a product"
const NOT_ABLE_TO_DELETE_A_PRODUCT = "was not able to delete a product"
const NOT_ABLE_TO_PING = "was not able to ping the repository"

// Level of importance of an Error
type Intensity int

const (
	// The operation completly failed
	Strong Intensity = iota
	// The operation partialy failed or their is only a warning
	Weak
)

type BatchError map[*domain.Product]*Error

func (b BatchError) HasStrongError() bool {
	for _, v := range b {
		if v.Intensity == Strong {
			return true
		}
	}
	return false
}

func (b BatchError) Len() uint {
	return uint(len(b))
}

func (b BatchError) Error() string {
	msg := ""
	for _, v := range b {
		msg += v.Error()
		msg += "\n"
	}
	return msg
}

func (b BatchError) Get(p *domain.Product) *Error {
	return b[p]
}
