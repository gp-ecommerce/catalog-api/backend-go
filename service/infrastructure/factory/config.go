package factory

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure/implementation"
)

type Implementation string

const (
	Mongo Implementation = "mongo"
)

func provideRepository(impl Implementation) (infrastructure.Repository, error) {
	switch impl {
	case Mongo:
		{
			addr := fmt.Sprintf("mongodb://localhost:%v", currentPort)
			return implementation.ProvideMongoRepository(addr)
		}

	}
	return nil, fmt.Errorf("the implementation don't exist")
}

func SetConfigFromDocker() error {
	viper.SetConfigName("docker-compose.yml")
	viper.AddConfigPath("./../../../")
	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	ports := viper.GetString("services.repo.ports")
	splittedPort := strings.Split(":", ports)
	port := splittedPort[0]
	currentPort = fmt.Sprintf("repo:%v", port)

	nameImplementations := viper.GetString("services.repo.image")
	splittedNameImplementations := strings.Split(":", nameImplementations)
	currentImplementation = Implementation(splittedNameImplementations[0])

	return nil
}

func GetPort() string {
	return currentPort
}

func GetImplementation() Implementation {
	return currentImplementation
}
