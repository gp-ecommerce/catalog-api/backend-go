package factory

import "gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure"

func ProvideRepository() (infrastructure.Repository, error) {
	if repository == nil {
		return provideRepository(currentImplementation)
	}
	return repository, nil
}
