package implementation

import (
	"context"
	"fmt"
	"math"
	"time"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/domain"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type mongoRepository struct {
	addr      string
	inventory *mongo.Collection
}

const _database = "catalogue"
const _collection = "inventory"

func ProvideMongoRepository(addr string) (*mongoRepository, error) {
	driver := mongoRepository{addr: addr}
	disconnect, _, client, err := driver.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	database := client.Database(_database)
	inventory := database.Collection(_collection)

	driver = mongoRepository{addr: addr, inventory: inventory}
	err = driver.setIndexMongo()
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_INSTANTIATE_THE_REPOSITORY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return &driver, nil
}

func (m mongoRepository) GetByShopCategory(category string, limit uint) ([]domain.Product, error) {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Intensity: infrastructure.Strong,
		}
	}
	opt := options.Find()
	opt.SetLimit(int64(limit))

	query := bson.M{"categories": bson.M{"$all": bson.A{category}}}
	cursor, err := m.inventory.Find(ctx, query, opt)

	if err == mongo.ErrNoDocuments {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NO_RESULT_FROM_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Weak,
		}
	} else if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return cursorToProduct(cursor, limit, ctx)
}

func (m mongoRepository) GetByName(name string, limit uint) ([]domain.Product, error) {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	opt := options.Find()
	opt.SetLimit(int64(limit))

	query := bson.M{"product.name": name}
	cursor, err := m.inventory.Find(ctx, query, opt)
	if err == mongo.ErrNoDocuments {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NO_RESULT_FROM_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Weak,
		}
	} else if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return cursorToProduct(cursor, limit, ctx)
}

func (m mongoRepository) GetByID(id string) (domain.Product, error) {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return domain.Product{}, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}

	}
	query := bson.M{"product.id": id}
	mongoResult := mongoProduct{}
	err = m.inventory.FindOne(ctx, query).Decode(&mongoResult)
	if err == mongo.ErrNoDocuments {
		return domain.Product{}, &infrastructure.Error{
			Message:   infrastructure.NO_RESULT_FROM_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Weak,
		}
	} else if err != nil {
		return domain.Product{}, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return mongoResult.Product, nil
}

func (m mongoRepository) SearchName(name string, limit uint) ([]domain.Product, error) {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	opt := options.Find()
	opt.SetLimit(int64(limit))

	query := bson.M{"product.name": bson.M{"$regex": fmt.Sprintf("^%v", name)}}
	cursor, err := m.inventory.Find(ctx, query, opt)
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return cursorToProduct(cursor, limit, ctx)
}

func (m mongoRepository) AddProducts(products ...*domain.Product) error {
	errorBatch := infrastructure.BatchError{}
	for _, p := range products {
		err := m.addAProduct(p)
		if err != nil {
			errorBatch[p] = err.(*infrastructure.Error)
		}
	}
	if len(errorBatch) == 0 {
		return nil
	}
	return &errorBatch
}

func (m mongoRepository) DeleteProducts(products ...*domain.Product) error {
	errorBatch := infrastructure.BatchError{}
	for _, p := range products {
		err := m.deleteAProduct(p)
		if err != nil {
			errorBatch[p] = err.(*infrastructure.Error)
		}
	}
	if len(errorBatch) == 0 {
		return nil
	}
	return &errorBatch
}

func (m mongoRepository) AddCategories(product *domain.Product, categories ...string) error {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	updateStatement := bson.M{"$push": bson.M{"categories": bson.M{"$each": categories}}}
	filter := bson.M{"product.id": product.Id}
	_, err = m.inventory.UpdateOne(ctx, filter, updateStatement)
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_ADD_CATEGORIES,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}

	}
	return nil

}

func (m mongoRepository) RemoveCategories(product *domain.Product, categories ...string) error {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}

	updateStatement := bson.M{"$pull": bson.M{"categories": bson.M{"$in": categories}}}
	filter := bson.M{"product.id": product.Id}
	_, err = m.inventory.UpdateOne(ctx, filter, updateStatement)
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_REMOVE_CATEGORIES,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return nil
}

func (m mongoRepository) ClearCategories(product *domain.Product) error {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	filter := bson.M{"product.id": product.Id}
	updateStatement := bson.M{"$set": bson.M{"categories": bson.A{}}}
	_, err = m.inventory.UpdateOne(ctx, filter, updateStatement)
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CLEAR_CATEGORIES,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return nil
}

func (m mongoRepository) Len() (uint, error) {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return 0, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	size, err := m.inventory.CountDocuments(ctx, bson.M{})
	if err != nil {
		return 0, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return uint(size), nil
}

func (m mongoRepository) Clear() error {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	_, err = m.inventory.DeleteMany(ctx, bson.M{})
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CLEAR_REPOSITORY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return nil
}

func (m mongoRepository) All() ([]domain.Product, error) {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}

	cursor, err := m.inventory.Find(ctx, bson.M{})
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return cursorToProduct(cursor, math.MaxUint8, ctx)
}

func (m mongoRepository) GetProductCategories(product *domain.Product) ([]string, error) {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}

	}
	query := bson.M{"product.id": product.Id}
	mongoResult := mongoProduct{}
	err = m.inventory.FindOne(ctx, query).Decode(&mongoResult)
	if err == mongo.ErrNoDocuments {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NO_RESULT_FROM_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Weak,
		}
	} else if err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_QUERY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return mongoResult.Categories, nil
}

func (m mongoRepository) Ping() error {
	disconnect, ctx, client, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_PING,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	return nil

}

func (m mongoRepository) connectToDb() (func(), context.Context, *mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(m.addr))

	return func() {
		cancel()
	}, ctx, client, err
}

func cursorToProduct(cursor *mongo.Cursor, limit uint, ctx context.Context) ([]domain.Product, error) {
	productsRes := make([]mongoProduct, 0, limit)
	if err := cursor.All(ctx, &productsRes); err != nil {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_INSTANTIATE_THE_REPOSITORY,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	products := make([]domain.Product, len(productsRes))
	if len(productsRes) == 0 {
		return nil, &infrastructure.Error{
			Message:   infrastructure.NO_RESULT_FROM_QUERY,
			Intensity: infrastructure.Weak,
		}
	}
	for i, p := range productsRes {
		products[i] = p.Product
	}
	return products, nil
}

func (m mongoRepository) setIndexMongo() error {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return err
	}
	indexes := []mongo.IndexModel{
		{
			Keys: bson.D{
				{"product.name", "text"},
			}, Options: nil,
		},
		{
			Keys: bson.D{
				{"categories", 1},
			}, Options: nil,
		},
		{
			Keys: bson.D{
				{"product.id", 1},
			}, Options: nil,
		},
	}
	_, err = m.inventory.Indexes().CreateMany(ctx, indexes)
	if err != nil {
		return err
	}

	return nil
}

func (m mongoRepository) addAProduct(product *domain.Product) error {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	_, err = m.GetByID(product.Id)
	if err == nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_ADD_A_PRODUCT,
			Detail:    "A product with the same ID already exist",
			Intensity: infrastructure.Weak,
		}
	}
	if err != nil {
		errProduct := err.(*infrastructure.Error)
		if errProduct.Intensity == infrastructure.Strong {
			return &infrastructure.Error{
				Message:   infrastructure.NOT_ABLE_TO_ADD_A_PRODUCT,
				Detail:    errProduct.Detail,
				Intensity: infrastructure.Strong,
			}
		}
	}

	_, err = m.inventory.InsertOne(ctx,
		mongoProduct{Product: *product,
			Categories: []string{}})
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_ADD_A_PRODUCT,
			Detail:    err.Error(),
			Intensity: infrastructure.Weak,
		}
	}
	return nil
}

func (m mongoRepository) deleteAProduct(product *domain.Product) error {
	disconnect, ctx, _, err := m.connectToDb()
	defer disconnect()
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_CONNECT,
			Detail:    err.Error(),
			Intensity: infrastructure.Strong,
		}
	}
	filter := bson.M{"product.id": product.Id}
	count, err := m.inventory.DeleteOne(ctx, filter)
	if err != nil {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_DELETE_A_PRODUCT,
			Detail:    err.Error(),
			Intensity: infrastructure.Weak,
		}
	}
	if count.DeletedCount == 0 {
		return &infrastructure.Error{
			Message:   infrastructure.NOT_ABLE_TO_DELETE_A_PRODUCT,
			Detail:    "the product don't exist in the repository",
			Intensity: infrastructure.Weak,
		}
	}
	return nil
}

type mongoProduct struct {
	domain.Product `bson:"product"`
	Categories     []string `bson:"categories"`
}
