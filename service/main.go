package main

import (
	"net/http"
	"os"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/server"
)

func main() {
	srv := &http.Server{}
	close := make(chan os.Signal, 1)
	server.BuildServer(srv, close)
}
