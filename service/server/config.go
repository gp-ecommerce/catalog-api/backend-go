package server

import (
	"strings"

	"github.com/spf13/viper"
)

var currentPort = "80"

func SetPortFromDocker() error {
	viper.SetConfigName("docker-compose.yml")
	viper.AddConfigPath("./../../")
	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	ports := viper.GetString("services.api.ports")
	splittedPort := strings.Split(":", ports)
	port := splittedPort[0]
	currentPort = port

	return nil
}

func GetPort() string {
	return currentPort
}
