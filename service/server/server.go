package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	service "gitlab.com/gp-ecommerce/catalog-api/backend-go/service/api"
)

func setRoute() {
	http.HandleFunc("/graphql", service.GraphQl)
}

func getPort() string {

	return ":" + GetPort()
}

func BuildServer(srv *http.Server, close chan os.Signal) {

	srv.Addr = getPort()
	setRoute()
	signal.Notify(close, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	log.Println("Server started")
	<-close
	closeServer(srv)
}

// send a close server signal
func SendCloseSignal(close chan os.Signal) {
	close <- os.Interrupt
}

func closeServer(srv *http.Server) {
	log.Print("Server Stopped")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
	log.Print("Server Exited Properly")
}
