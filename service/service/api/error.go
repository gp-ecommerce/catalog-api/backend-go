package api

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure"
	service_graphql "gitlab.com/gp-ecommerce/catalog-api/backend-go/service/graphql"
)

func sendError(e error, res http.ResponseWriter) {
	log.Printf("%#v", e)
	res.Header().Set("Content-Type", "application/json")
	if err, ok := e.(service_graphql.Error); ok {
		findErrorCode(&err, res)
		sendJsonError(&err, res)
	} else {
		res.WriteHeader(http.StatusInternalServerError)
		b, err := json.Marshal(e)
		if err == nil {
			log.Println("was not able to serialize error")
		}
		res.Write(b)
	}
}

func findErrorCode(e *service_graphql.Error, res http.ResponseWriter) {
	for _, err := range e.FormattedError {
		if infraErr, ok := err.OriginalError().(infrastructure.Error); ok {
			if infraErr.Intensity == infrastructure.Strong {
				res.WriteHeader(http.StatusInternalServerError)
				break
			} else {
				res.WriteHeader(http.StatusBadRequest)
			}
		}
	}
	res.WriteHeader(http.StatusInternalServerError)
}

func sendJsonError(e *service_graphql.Error, res http.ResponseWriter) {
	b, err := json.Marshal(e)
	if err == nil {
		log.Println("was not able to serialize error")
	}
	res.Write(b)
}
