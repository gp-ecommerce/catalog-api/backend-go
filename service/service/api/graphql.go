package api

import (
	"encoding/json"
	"net/http"

	service_graphql "gitlab.com/gp-ecommerce/catalog-api/backend-go/service/graphql"
)

func GraphQl(w http.ResponseWriter, r *http.Request) {
	res, err := service_graphql.ExecuteQuery(r.URL.Query().Get("query"))
	if err != nil {
		sendError(err, w)
		return
	}
	json.NewEncoder(w).Encode(res)
}
