package service_graphql

import (
	"github.com/graphql-go/graphql/gqlerrors"
)

type Error struct {
	FormattedError []gqlerrors.FormattedError
}

func (e Error) Error() string {
	msg := ""
	for _, err := range e.FormattedError {
		msg += err.Message + "\n"
	}
	return msg
}
