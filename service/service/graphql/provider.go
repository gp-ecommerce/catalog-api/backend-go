package service_graphql

import (
	"github.com/graphql-go/graphql"
)

var graphQlSchema *graphql.Schema = nil

func provideSchema() (*graphql.Schema, error) {
	if graphQlSchema == nil {
		schema, err := graphql.NewSchema(
			graphql.SchemaConfig{
				Query:    queryType,
				Mutation: nil,
			},
		)
		if err != nil {
			return nil, err
		}
		graphQlSchema = &schema
	}

	return graphQlSchema, nil
}

func ExecuteQuery(query string) (*graphql.Result, error) {
	schema, err := provideSchema()
	if err != nil {
		return nil, err
	}
	result := graphql.Do(graphql.Params{
		Schema:        *schema,
		RequestString: query,
	})
	if len(result.Errors) > 0 {
		return nil, Error{FormattedError: result.Errors}
	}
	return result, nil
}
