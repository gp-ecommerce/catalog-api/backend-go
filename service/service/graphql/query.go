package service_graphql

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure/factory"
)

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"product": &graphql.Field{
				Type:        productType,
				Description: "get product by ID",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id, ok := p.Args["id"].(string)
					if ok {
						repo, err := factory.ProvideRepository()
						if err != nil {
							return nil, err
						}
						product, err := repo.GetByID(id)
						if err != nil {
							return nil, err
						}
						return product, nil
					}
					return nil, nil
				},
			},
		},
	})
