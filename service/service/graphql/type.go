package service_graphql

import "github.com/graphql-go/graphql"

var productType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "product",
		Fields: graphql.Fields{
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"description": &graphql.Field{
				Type: graphql.String,
			},
			"short_description": &graphql.Field{
				Type: graphql.String,
			},
		},
	},
)
