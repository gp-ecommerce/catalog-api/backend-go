package api_test

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/domain"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure/factory"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/service/api"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/test/helper"
)

func TestMain(m *testing.M) {
	repo, err := factory.ProvideRepository()
	if err != nil {
		log.Panic(err)
	}
	err = repo.Clear()
	if err != nil {
		log.Panic(err)
	}
	os.Exit(m.Run())
}

func TestGetAProduct(t *testing.T) {
	id := "foo"
	product := &domain.Product{
		Id:               id,
		Name:             "bar",
		Description:      "test",
		ShortDescription: "short test",
	}
	helper.AddProductsToRepo(t, product)
	ts := httptest.NewServer(http.HandlerFunc(api.GraphQl))
	defer ts.Close()

	query := fmt.Sprintf("%v?query={product(id:\"%v\"){name,description,id,short_description}}", ts.URL, id)

	res, err := http.Get(query)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("the status was not sucessfull (200) but was %v ", res.StatusCode)
	}

	resp, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	var graphqlRes helper.GraphqlData

	err = json.Unmarshal(resp, &graphqlRes)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(*product, graphqlRes.Data.Product) {
		t.Errorf(`
		the product are not equal
		the initial product was "%v"
		the return product is "%v"
		`, product, graphqlRes.Data)
	}
}

func TestGetAProductWhenTheProductDontExist(t *testing.T) {
	id := "foo"
	product := &domain.Product{
		Id:               id,
		Name:             "bar",
		Description:      "test",
		ShortDescription: "short test",
	}
	helper.AddProductsToRepo(t, product)
	ts := httptest.NewServer(http.HandlerFunc(api.GraphQl))
	defer ts.Close()

	query := fmt.Sprintf("%v?query={product(id:\"%v\"){name,description,id,short_description}}", ts.URL, "do not exist")

	res, err := http.Get(query)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("should provide a bad request code (400) but return %v", res.StatusCode)
	}
}
