package graphql_test

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"reflect"
	"testing"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/domain"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure/factory"
	service "gitlab.com/gp-ecommerce/catalog-api/backend-go/service/graphql"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/test/helper"
)

func TestMain(m *testing.M) {
	repo, err := factory.ProvideRepository()
	if err != nil {
		log.Panic(err)
	}
	err = repo.Clear()
	if err != nil {
		log.Panic(err)
	}
	os.Exit(m.Run())
}
func TestGetProductById(t *testing.T) {
	id := "foo"
	product := domain.Product{
		Id:               id,
		Name:             "bar",
		Description:      "test",
		ShortDescription: "short test",
	}
	repo := helper.ProvideRepository(t)

	err := repo.AddProducts(&product)
	if err != nil {
		t.Fatal(err)
	}
	query := fmt.Sprintf("{product(id:\"%v\"){name,description,id,short_description}}", id)
	res, err := service.ExecuteQuery(query)
	if err != nil {
		t.Fatal(err)
	}
	b, err := json.Marshal(res)
	if err != nil {
		t.Fatal(err)
	}
	var graphqlRes helper.GraphqlData

	err = json.Unmarshal(b, &graphqlRes)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(product, graphqlRes.Data.Product) {
		t.Errorf(`
		the product are not equal
		the initial product was "%v"
		the return product is "%v"
		`, product, graphqlRes.Data)
	}

}

func TestGetProductThatDoesntExist(t *testing.T) {
	id := "foo"
	product := domain.Product{
		Id:               id,
		Name:             "bar",
		Description:      "test",
		ShortDescription: "short test",
	}
	repo := helper.ProvideRepository(t)

	err := repo.AddProducts(&product)
	if err != nil {
		t.Fatal(err)
	}
	query := fmt.Sprintf("{product(id:\"%v\"){name,description,id,short_description}}", "i do not exist")
	_, err = service.ExecuteQuery(query)
	if err == nil {
		t.Fatalf("should return an error")
	}

}

func TestGetProductByIdWithPartialEntries(t *testing.T) {
	id := "foo"
	product := domain.Product{
		Id:               id,
		Name:             "bar",
		Description:      "test",
		ShortDescription: "short test",
	}
	repo := helper.ProvideRepository(t)

	err := repo.AddProducts(&product)
	if err != nil {
		t.Fatal(err)
	}
	baseQuery := "{product(id:\"%v\"){%v}}"

	queries := []helper.PairQueryProduct{
		{
			Query:   fmt.Sprintf(baseQuery, id, "name"),
			Product: domain.Product{Name: product.Name},
		},
		{
			Query:   fmt.Sprintf(baseQuery, id, "name,description"),
			Product: domain.Product{Name: product.Name, Description: product.Description},
		},
		{
			Query:   fmt.Sprintf(baseQuery, id, "id"),
			Product: domain.Product{Id: product.Id},
		},
		{
			Query:   fmt.Sprintf(baseQuery, id, "id,short_description"),
			Product: domain.Product{Id: product.Id, ShortDescription: product.ShortDescription},
		},
	}

	for _, q := range queries {
		res, err := service.ExecuteQuery(q.Query)
		if err != nil {
			t.Fatal(err)
		}
		b, err := json.Marshal(res)
		if err != nil {
			t.Fatal(err)
		}
		var graphqlRes helper.GraphqlData

		err = json.Unmarshal(b, &graphqlRes)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(q.Product, graphqlRes.Data.Product) {
			t.Errorf(`
		the product are not equal
		the initial product was "%v"
		the return product is "%v"
		`, product, graphqlRes.Data)
		}
	}

}
