package helper

import (
	"testing"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/domain"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure/factory"
)

func ProvideRepository(t *testing.T) infrastructure.Repository {
	repo, err := factory.ProvideRepository()
	if err != nil {
		t.Fatal(err)
	}
	t.Cleanup(func() {
		err := repo.Clear()
		if err != nil {
			t.Fatal(err)
		}
	})
	return repo
}

type GraphqlData struct {
	Data GraphQlProduct `json:"data"`
}

type GraphQlProduct struct {
	Product domain.Product `json:"product"`
}

type PairQueryProduct struct {
	Query   string
	Product domain.Product
}

func AddProductsToRepo(t *testing.T, products ...*domain.Product) {
	repo := ProvideRepository(t)

	err := repo.AddProducts(products...)
	if err != nil {
		t.Fatal(err)
	}

}
