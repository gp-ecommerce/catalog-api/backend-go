package json_test

import (
	"testing"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure/implementation"
	infrastructure_test "gitlab.com/gp-ecommerce/catalog-api/backend-go/test/infrastructure"
)

const _debug_mongo_addr = "mongodb://localhost:27017"

func TestMongoRepository(t *testing.T) {
	_, err := implementation.ProvideMongoRepository(_debug_mongo_addr)
	if err == nil {
		repo, err := implementation.ProvideMongoRepository(_debug_mongo_addr)
		if err != nil {
			t.Fatalf("%v", err)

		}
		if err := repo.Clear(); err != nil {
			t.Fatalf("was not able to clear to repository. Because error '%v'", err)
		}
		for _, testCase := range infrastructure_test.ProvideRepositoryTest() {
			repo, err := implementation.ProvideMongoRepository(_debug_mongo_addr)
			if err != nil {
				t.Fatalf("%v", err)
			}
			testFunction, name := testCase(repo)
			t.Run(name, testFunction)
			if err := repo.Clear(); err != nil {
				t.Fatalf("was not able to clear to repository. Because error '%v'", err)
			}
		}
	} else {
		t.Fatalf("was not able to instantiate the database all the test were skip `%v`", err)
	}

}
