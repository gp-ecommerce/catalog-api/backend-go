package infrastructure_test

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/gp-ecommerce/catalog-api/backend-go/domain"
	"gitlab.com/gp-ecommerce/catalog-api/backend-go/infrastructure"
)

func ProvideRepositoryTest() []func(infrastructure.Repository) (func(*testing.T), string) {
	return []func(infrastructure.Repository) (func(*testing.T), string){
		testAddAProduct,
		testAddMultipleProducts,
		testAddAProductWhenAnotherExistWithTheSameId,
		testAddProductsInABatchWithProductWithIdenticalId,

		testGetAProductById,
		testGetAProductByIdNoCandidate,

		testGetAProductByName,
		testGetAProductByNameNoCandidate,
		testGetAProductByNameWithMultipleCandidate,
		testGetAProductByNameWithMultipleCandidateWithAShortLimit,

		testAddACategory,
		testAddMultipleCategories,
		testGetProductByShopCategoriesWithNoCandidate,
		testGetProductByShopCategoriesWithMultipleCandidates,
		testGetProductByShopCategoriesWithMultipleCandidatesShortLimit,
		testClearCategories,
		testGetCategoriesOfProduct,
		testRemoveCategories,

		testRemoveAProduct,
		testRemoveMultipleProducts,
		testRemoveAProductThatIsNotPresent,
		testRemoveMultipleProductsWithProductThatDontExist,

		testSearch,
	}
}
func testAddAProduct(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		product := domain.Product{
			Id:   "foo",
			Name: "bar",
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}

		size, err := repo.Len()
		if err != nil {
			t.Fatal(err)
		}
		if size != 1 {
			t.Fatalf("the size of the repository is not of one")
		}

	}, "TestAddAProduct"

}

func testAddMultipleProducts(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		numberOfProduct := 100
		productsToAdd := generateProductsWithId(numberOfProduct)

		batchErr := repo.AddProducts(productsToAdd...)
		if batchErr != nil {
			t.Fatal(batchErr)
		}

		size, err := repo.Len()
		if err != nil {
			t.Fatal(err)
		}
		if size != uint(numberOfProduct) {
			t.Fatalf("the size of the repository is not of %v, but of %v", numberOfProduct, size)
		}
		products, err := repo.All()
		if err != nil {
			t.Fatal(err)
		}
		if !haveTheSameProduct(productsToAdd, products) {
			t.Fatalf("does not have the same product  \n product:`%v`\n product in repository : `%v`", productsToAdd, products)
		}

	}, "TestAddMultipleProducts"
}

func testAddAProductWhenAnotherExistWithTheSameId(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		product := domain.Product{
			Id:   "foo",
			Name: "bar",
		}
		err := repo.AddProducts(&product)
		if err != nil {
			t.Fatal(err)
		}
		secondProduct := domain.Product{
			Id:   "foo",
			Name: "bar2",
		}

		err = repo.AddProducts(&secondProduct)
		batchError, ok := err.(*infrastructure.BatchError)
		if !ok {
			t.Fatal("not able to convert the error into BatchError")
		}
		if batchError.Get(&secondProduct).Message != infrastructure.NOT_ABLE_TO_ADD_A_PRODUCT {
			t.Fatal("should return an error")
		}
	}, "TestAddAProductWhenAnotherExistWithTheSameId"
}

func testAddProductsInABatchWithProductWithIdenticalId(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		identicalId := "foo"
		numberOfIdenticalIdProduct := 100
		numberOfValidProduct := 100
		productWithIdenticalId := make([]*domain.Product, 0, numberOfIdenticalIdProduct)
		for i := 0; i < numberOfIdenticalIdProduct; i++ {
			productWithIdenticalId = append(productWithIdenticalId, &domain.Product{Id: identicalId})
		}
		products := make([]*domain.Product, 0, numberOfIdenticalIdProduct+numberOfValidProduct)
		products = append(products, productWithIdenticalId...)
		products = append(products, generateProductsWithId(numberOfValidProduct)...)

		err := repo.AddProducts(products...)
		if err == nil {
			t.Fatal("should return an error")
		}
		batchErr, ok := err.(*infrastructure.BatchError)

		if !ok {
			t.Fatal("should be able to convert the error into BatchError")
		}

		if batchErr.Len() != uint(numberOfIdenticalIdProduct-1) {
			t.Fatalf("should have %v errors but has %v errors", numberOfIdenticalIdProduct-1, batchErr.Len())
		}

		if batchErr.HasStrongError() {
			t.Fatal("the error should not be strong")
		}

		for p, e := range *batchErr {
			if p.Id != identicalId {
				t.Fatalf("the id of the err product should be %v but is %v", identicalId, p.Id)
			}
			if e.Message != infrastructure.NOT_ABLE_TO_ADD_A_PRODUCT {
				t.Fatalf("should return NOT_ABLE_TO_ADD_A_PRODUCT but return %v", e.Message)
			}
		}

	}, "testAddProductsInABatchWithProductWithIdenticalId"
}
func testGetAProductById(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		id := "foo"
		name := "bar"
		product := domain.Product{
			Id:   id,
			Name: name,
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		p, err := repo.GetByID(id)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(p, product) {
			t.Fatalf("the  product query by id are not equals.\n original: %v \n from repo: %v ", product, p)
		}
	}, "TestGetAProductById"
}

func testGetAProductByIdNoCandidate(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		id := "foo"
		p, err := repo.GetByID(id)
		if err == nil {
			t.Fatalf("should not return a result but returned `%v`", p)
		}
	}, "TestGetAProductByIdNoCandidate"
}

func testGetAProductByName(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		id := "foo"
		name := "bar"
		product := domain.Product{
			Id:   id,
			Name: name,
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		p, err := repo.GetByName(name, 1)
		if err != nil {
			t.Fatal(err)
		}
		if len(p) != 1 {
			t.Fatal("was suppose to return one result")
		}
		if !reflect.DeepEqual(p[0], product) {
			t.Fatalf("the  product query by id are not equals.\n original: %v \n from repo: %v ", product, p)
		}
	}, "TestGetAProductByName"
}

func testGetAProductByNameNoCandidate(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		name := "bar"

		_, err := repo.GetByName(name, 1)
		if err.(*infrastructure.Error).Message != infrastructure.NO_RESULT_FROM_QUERY {
			t.Fatal("suppose to return no result")
		}

	}, "TestGetAProductByNameNoCandidate"
}

func testGetAProductByNameWithMultipleCandidate(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		name := "foo"
		invalidName := "bar"
		productsWithValidName := []*domain.Product{
			{Name: name, Id: "1"},
			{Name: name, Id: "2"},
			{Name: name, Id: "3"},
		}
		productsWithInvalidName := []*domain.Product{
			{Name: invalidName, Id: "4"},
			{Name: invalidName, Id: "5"},
			{Name: invalidName, Id: "6"},
			{Name: "pong", Id: "abc"},
		}

		products := make([]*domain.Product, 0, len(productsWithValidName)+len(productsWithInvalidName))
		products = append(products, productsWithValidName...)
		products = append(products, productsWithInvalidName...)

		err := repo.AddProducts(products...)
		if err != nil {
			t.Fatal(err)
		}

		p, errGet := repo.GetByName(name, 10)
		if errGet != nil {
			t.Fatal(err)
		}

		if !haveTheSameProduct(productsWithValidName, p) {
			t.Fatalf("does not have the same product  \n product:`%v`\n product in repository : `%v`", productsWithValidName, p)
		}

	}, "TestGetAProductByNameWithMultipleCandidate"
}

func testGetAProductByNameWithMultipleCandidateWithAShortLimit(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		name := "foo"
		invalidName := "bar"
		limit := 2
		productsWithValidName := []*domain.Product{
			{Name: name, Id: "1"},
			{Name: name, Id: "2"},
			{Name: name, Id: "3"},
			{Name: name, Id: "10"},
		}
		productsWithInvalidName := []*domain.Product{
			{Name: invalidName, Id: "4"},
			{Name: invalidName, Id: "5"},
			{Name: invalidName, Id: "6"},
			{Name: "pong", Id: "abc"},
		}

		products := make([]*domain.Product, 0, len(productsWithValidName)+len(productsWithInvalidName))
		products = append(products, productsWithValidName...)
		products = append(products, productsWithInvalidName...)

		err := repo.AddProducts(products...)
		if err != nil {
			t.Fatal(err)
		}

		res, errGet := repo.GetByName(name, uint(limit))
		if errGet != nil {
			t.Fatal(err)
		}
		if size := len(res); size != limit {
			t.Fatalf("the size should be %v but is %v", limit, size)
		}
		for _, p := range res {
			if p.Name != name {
				t.Fatalf("a result returned don't have the right name `%v` but has the name `%v`", name, p.Name)
			}
		}
	}, "testGetAProductByNameWithMultipleCandidateAndAShortLimit"
}

func testAddACategory(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		id := "foo"
		category := "chat"
		product := domain.Product{
			Id:   id,
			Name: "bar",
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		err := repo.AddCategories(&product, category)
		if err != nil {
			t.Fatal(err)
		}
		p, err := repo.GetByShopCategory(category, 100)
		if err != nil {
			t.Fatal(err)
		}
		if len(p) != 1 {
			t.Fatalf("should return a product but has `%v`", len(p))
		}
		if !reflect.DeepEqual(p[0], product) {
			t.Fatalf(`The product are not the same
			original product "%v"
			product from repository "%v"`, product, p[0])
		}

	}, "testAddACategory"
}

func testAddMultipleCategories(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		id := "foo"
		categories := []string{"chat", "dog", "food", "fish"}
		product := domain.Product{
			Id:   id,
			Name: "bar",
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		err := repo.AddCategories(&product, categories...)
		if err != nil {
			t.Fatal(err)
		}
		for _, c := range categories {
			p, err := repo.GetByShopCategory(c, 1)
			if err != nil {
				t.Fatal(err)
			}
			if len(p) != 1 {
				t.Fatalf("should return a product but has `%v`", len(p))
			}
			if !reflect.DeepEqual(p[0], product) {
				t.Fatalf(`The product are not the same
			original product "%v"
			product from repository "%v"`, product, p[0])
			}
		}

	}, "TestAddMultipleCategories"
}

func testGetProductByShopCategoriesWithNoCandidate(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		id := "foo"
		categories := []string{"chat", "dog", "food", "fish"}
		product := domain.Product{
			Id:   id,
			Name: "bar",
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		err := repo.AddCategories(&product, categories...)
		if err != nil {
			t.Fatal(err)
		}
		p, err := repo.GetByShopCategory("...", 1)
		if err == nil {
			t.Errorf("should return no result but returned `%v`", p)
		} else {
			if err.(*infrastructure.Error).Message != infrastructure.NO_RESULT_FROM_QUERY {
				t.Fatalf("was suppose to return `%v` but return an error with `%v`", infrastructure.NO_RESULT_FROM_QUERY, err.Error())
			}

		}
	}, "TestGetProductByShopCategoriesWithNoCandidate"
}

func testGetProductByShopCategoriesWithMultipleCandidates(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		products := []*domain.Product{
			{Id: "1"},
			{Id: "2"},
			{Id: "3"},
			{Id: "4"},
		}
		categories := [][]string{
			{"foo", "bar"},
			{"foo"},
			{"bar"},
			{},
		}
		err := repo.AddProducts(products...)
		if err != nil {
			t.Fatal(err)
		}

		for i, c := range categories {
			err := repo.AddCategories(products[i], c...)
			if err != nil {
				t.Fatal(err)
			}
		}

		p, err := repo.GetByShopCategory("foo", 10)
		if err != nil {
			t.Fatal(err)
		}
		shouldReturn := products[0:2]
		if !haveTheSameProduct(shouldReturn, p) {
			t.Errorf(`all the products from the category "%v" are not present
			the query should return "%v"
			but return "%v"`, "foo", products[0:2], p)
		}

		p, err = repo.GetByShopCategory("bar", 10)
		if err != nil {
			t.Fatal(err)
		}
		shouldReturn = []*domain.Product{
			products[0],
			products[2],
		}
		if !haveTheSameProduct(shouldReturn, p) {
			t.Errorf(`all the products from the category "%v" are not present
			the query should return "%v"
			but return "%v"`, "bar", shouldReturn, p)
		}
	}, "TestGetProductByShopCategoriesWithMultipleCandidates"
}

func testGetProductByShopCategoriesWithMultipleCandidatesShortLimit(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		numberOfProduct := 100
		limit := 20
		category := "foo"
		products := generateProductsWithId(numberOfProduct)
		err := repo.AddProducts(products...)
		if err != nil {
			t.Fatal(err)
		}
		for _, p := range products {
			err = repo.AddCategories(p, category)
			if err != nil {
				t.Fatal(err)
			}
		}
		res, err := repo.GetByShopCategory(category, uint(limit))
		if err != nil {
			t.Fatal(err)
		}
		if len(res) != limit {
			t.Errorf("should return `%v` results but has `%v`", limit, len(res))
		}

	}, "TestGetProductByShopCategoriesWithMultipleCandidatesShortLimit"
}

func testClearCategories(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		categories := []string{"chat", "dog", "food", "fish"}
		product := domain.Product{
			Id:   "foo",
			Name: "bar",
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		err := repo.AddCategories(&product, categories...)
		if err != nil {
			t.Fatal(err)
		}

		err = repo.ClearCategories(&product)
		if err != nil {
			t.Fatal(err)
		}

		res, err := repo.GetProductCategories(&product)
		if err != nil {
			t.Fatal(err)
		}
		if len(res) != 0 {
			t.Fatalf("the product should have no category but has `%v`", res)
		}

	}, "TestClearCategories"
}

func testGetCategoriesOfProduct(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		categories := []string{"chat", "dog", "food", "fish"}
		product := domain.Product{
			Id:   "foo",
			Name: "bar",
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		err := repo.AddCategories(&product, categories...)
		if err != nil {
			t.Fatal(err)
		}

		res, err := repo.GetProductCategories(&product)
		if err != nil {
			t.Fatal(err)
		}
		if !haveTheSameCategories(categories, res) {
			t.Fatalf(`don't have the same categories
			initially has "%v"
			repository return "%v"`, categories, res)
		}

	}, "TestGetCategoriesOfProduct"
}

func testRemoveCategories(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		categoriesToRemove := []string{"chat", "food"}
		remaningCategories := []string{"dog", "fish"}
		categories := make([]string, 0, len(categoriesToRemove)+len(remaningCategories))
		categories = append(categories, categoriesToRemove...)
		categories = append(categories, remaningCategories...)

		product := domain.Product{
			Id:   "foo",
			Name: "bar",
		}
		batchErr := repo.AddProducts(&product)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		err := repo.AddCategories(&product, categories...)
		if err != nil {
			t.Fatal(err)
		}

		err = repo.RemoveCategories(&product, categoriesToRemove...)
		if err != nil {
			t.Fatal(err)
		}
		res, err := repo.GetProductCategories(&product)
		if err != nil {
			t.Fatal(err)
		}
		if !haveTheSameCategories(remaningCategories, res) {
			t.Fatalf(`don't have the same categories
			suppose to have "%v"
			repository return "%v"`, remaningCategories, res)
		}

	}, "TestRemoveCategories"
}

func testRemoveAProduct(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		id := "foo"
		product := domain.Product{
			Id:   id,
			Name: "bar",
		}
		secondProduct := domain.Product{
			Id:   "too",
			Name: "bar",
		}
		batchErr := repo.AddProducts(&product, &secondProduct)
		if batchErr != nil {
			t.Fatal(batchErr)
		}
		err := repo.DeleteProducts(&secondProduct)
		if err != nil {
			t.Fatal(err)
		}

		size, err := repo.Len()
		if err != nil {
			t.Fatal(err)
		}
		if size != 1 {
			t.Fatalf("the size of the repository is not of one but of `%v`", size)
		}

		p, err := repo.GetByID(id)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(p, product) {
			t.Fatalf("the  product query by id are not equals.\n original: %v \n from repo: %v ", product, p)
		}

	}, "testRemoveCategories"
}

func testRemoveMultipleProducts(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		productsRemaining := generateProductsWithId(100)
		productsToDelete := []*domain.Product{
			{Id: "a"},
			{Id: "b"},
			{Id: "c"},
			{Id: "d"},
		}
		products := make([]*domain.Product, 0, len(productsRemaining)+len(productsToDelete))
		products = append(products, productsRemaining...)
		products = append(products, productsToDelete...)
		err := repo.AddProducts(products...)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.DeleteProducts(productsToDelete...)
		if err != nil {
			t.Fatal(err)
		}

		res, err := repo.All()
		if err != nil {
			t.Fatal(err)
		}
		if !haveTheSameProduct(productsRemaining, res) {
			t.Fatalf(`the products remaining are not expected
			should have "%v"
			but has "%v"
			`, productsRemaining, res)
		}
	}, "TestRemoveMultipleProducts"
}

func testRemoveMultipleProductsWithProductThatDontExist(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		productsRemaining := generateProductsWithId(100)
		nonExistantId := "foo"
		numberOfNonExistantProducts := 50
		productsToDelete := []*domain.Product{
			{Id: "a"},
			{Id: "b"},
			{Id: "c"},
			{Id: "d"},
		}
		productsThatDontExist := make([]*domain.Product, 0, numberOfNonExistantProducts)
		for i := 0; i < numberOfNonExistantProducts; i++ {
			productsThatDontExist = append(productsThatDontExist, &domain.Product{Id: nonExistantId})
		}
		products := make([]*domain.Product, 0, len(productsRemaining)+len(productsToDelete))
		products = append(products, productsRemaining...)
		products = append(products, productsToDelete...)
		err := repo.AddProducts(products...)
		if err != nil {
			t.Fatal(err)
		}
		productsToDelete = append(productsToDelete, productsThatDontExist...)
		err = repo.DeleteProducts(productsToDelete...)
		if err == nil {
			t.Fatal("should return an error")
		}

		batchErr, ok := err.(*infrastructure.BatchError)
		if !ok {
			t.Fatal("should be able to convert the error into BatchError")
		}
		if batchErr.Len() != uint(numberOfNonExistantProducts) {
			t.Fatalf("should have %v errors but has %v ", numberOfNonExistantProducts, batchErr.Len())
		}

		for p, e := range *batchErr {
			if p.Id != nonExistantId {
				t.Fatalf("the id of the err product should be %v but is %v", nonExistantId, p.Id)
			}
			if e.Message != infrastructure.NOT_ABLE_TO_DELETE_A_PRODUCT {
				t.Fatalf("should return NOT_ABLE_TO_ADD_A_PRODUCT but return %v", e.Message)
			}
		}

	}, "TestRemoveMultipleProductsWithProductThatDontExist"
}

func testRemoveAProductThatIsNotPresent(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		productToRemove := domain.Product{Id: "foo"}
		err := repo.DeleteProducts(&productToRemove)
		if err == nil {
			t.Fatal("should not be able to remove an product from an empty repository")
		}
		productToAdd := domain.Product{Id: "bar"}
		err = repo.AddProducts(&productToAdd)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.DeleteProducts(&productToRemove)
		if err == nil {
			t.Fatal("should not be able to remove a product that don't exist")
		}
	}, "TestRemoveAProductThatIsNotPresent"
}

func testSearch(repo infrastructure.Repository) (func(*testing.T), string) {
	return func(t *testing.T) {
		searchQuery := "foo"

		products := []*domain.Product{
			{
				Id:   "1",
				Name: "foo",
			},
			{
				Id:   "2",
				Name: "foo1",
			},
			{
				Id:   "3",
				Name: "foofoo",
			},
			{
				Id:   "4",
				Name: "bar",
			},
		}

		err := repo.AddProducts(products...)
		if err != nil {
			t.Fatal(err)
		}
		res, err := repo.SearchName(searchQuery, 10)
		if err != nil {
			t.Fatal(err)
		}

		if !haveTheSameProduct(products[0:3], res) {
			t.Fatalf(`
			should have "%v"
			but has "%v"
			`, products[0:3], res)
		}
	}, "TestSearch"
}
func generateProductsWithId(number int) []*domain.Product {
	products := make([]*domain.Product, 0, number)
	for i := 0; i < number; i++ {
		products = append(products, &domain.Product{Id: fmt.Sprintf("%v", i)})
	}
	return products
}

func haveTheSameProduct(added []*domain.Product, returned []domain.Product) bool {
	addedMap := map[string]domain.Product{}
	returnedMap := map[string]domain.Product{}
	for _, p := range added {
		addedMap[p.Id] = *p
	}

	for _, p := range returned {
		returnedMap[p.Id] = p
	}
	return reflect.DeepEqual(addedMap, returnedMap)
}

func haveTheSameCategories(added []string, returned []string) bool {
	addedMap := map[string]string{}
	returnedMap := map[string]string{}
	for _, p := range added {
		addedMap[p] = p
	}

	for _, p := range returned {
		returnedMap[p] = p
	}
	return reflect.DeepEqual(addedMap, returnedMap)
}
